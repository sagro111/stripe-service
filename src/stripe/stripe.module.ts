import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Stripe } from 'stripe';

export const STRIPE_TOKEN = 'STRIPE_TOKEN';
@Module({
  imports: [ConfigModule],
  providers: [
    {
      provide: STRIPE_TOKEN,
      useFactory: (): Stripe => {
        return new Stripe('sk_test_vamcsP2xioFJ2jBafwaUJF3s00WiUbulKs', {
          apiVersion: '2020-08-27',
        });
      },
    },
  ],
  exports: [STRIPE_TOKEN],
})
export class StripeModule {}
