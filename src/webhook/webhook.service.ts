import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';

@Injectable()
export class WebhookService {
  private readonly webhookKey: string;

  constructor(private readonly configService: ConfigService) {
    this.webhookKey = this.configService.get<string>('WEBHOOK_API_KEY');
  }

  // sig: string, rawBody: Buffer
  public async webhook() {
    let event;

    try {
      // event = this.stripeService.constructEvent(rawBody, sig, this.webhookKey);
    } catch (err) {
      return err;
    }

    switch (event.type) {
      case 'invoice.paid':
        // Used to provision services after the trial has ended.
        // The status of the invoice will show up as paid. Store the status in your
        // database to reference when a user accesses your service to avoid hitting rate limits.в
        console.log('paid');
        break;
      case 'invoice.payment_failed':
        // If the payment fails or the customer does not have a valid payment method,
        //  an invoice.payment_failed event is sent, the subscription becomes past_due.
        // Use this webhook to notify your user that their payment has
        // failed and to retrieve new card details.
        console.log('payment_failed');

        break;
      case 'customer.subscription.deleted':
        console.log('deleted');

        if (event.request != null) {
          // handle a subscription canceled by your request
          // from above.
        } else {
          // handle subscription canceled automatically based
          // upon your subscription settings.
        }
        break;
      default:
        // Unexpected event type
        console.log('defauilt');
    }
  }
}
