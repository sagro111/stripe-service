import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { STRIPE_TOKEN } from '../../stripe/stripe.module';
import { Stripe } from 'stripe';

@Injectable()
export class ProductsService {
  constructor(@Inject(STRIPE_TOKEN) private readonly stripe: Stripe) {}
  async config() {
    const prices = await this.stripe.prices.list({
      active: true,
      expand: ['data.product'],
    });

    const entities = prices.data
      .sort((a, b) => a.unit_amount - b.unit_amount)
      .map((price) => ({
        ...price,
        unit_amount_decimal: (price.unit_amount / 100).toFixed(2),
      }));

    return {
      status: HttpStatus.OK,
      message: 'Config success fetched',
      entities,
    };
  }
}
