import { Module } from '@nestjs/common';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';
import { StripeModule } from '../../stripe/stripe.module';

@Module({
  imports: [StripeModule],
  controllers: [ProductsController],
  providers: [ProductsService],
})
export class ProductsModule {}
