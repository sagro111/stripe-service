import { IsNotEmpty, IsString } from 'class-validator';

export class ListCreditCardsDto {
  @IsString()
  @IsNotEmpty()
  customerId: string;
}
