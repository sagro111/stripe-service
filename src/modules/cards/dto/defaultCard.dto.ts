import { IsNotEmpty, IsString } from 'class-validator';

export class SetDefaultCardDto {
  @IsNotEmpty()
  @IsString()
  paymentMethodId: string;

  @IsNotEmpty()
  @IsString()
  customerId: string;
}
