import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class AttachCardsDto {
  @IsString()
  @IsNotEmpty()
  paymentMethodId: string;

  @IsString()
  @IsNotEmpty()
  customerId: string;

  @IsBoolean()
  @IsOptional()
  hasDefaultCard: boolean;
}
