import { IsNotEmpty, IsString } from 'class-validator';

export class DetachCardDto {
  @IsString()
  @IsNotEmpty()
  paymentMethodId: string;

  @IsString()
  @IsNotEmpty()
  defaultPaymentMethodId: string | null;
}
