import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateChargeDto {
  @IsNotEmpty()
  @IsNumber()
  amount;

  @IsNotEmpty()
  @IsString()
  paymentMethodId;

  @IsNotEmpty()
  @IsString()
  customerId;
}
