import {
  BadRequestException,
  HttpStatus,
  Inject,
  Injectable,
  InternalServerErrorException,
  NotAcceptableException,
  NotFoundException,
} from '@nestjs/common';
import { AttachCardsDto } from './dto/attachCards.dto';
import { Stripe } from 'stripe';
import { ListCreditCardsDto } from './dto/listCreditCards.dto';
import { STRIPE_TOKEN } from '../../stripe/stripe.module';
import { DetachCardDto } from './dto/detachCard.dto';
import { SetDefaultCardDto } from './dto/defaultCard.dto';
import { StripeError } from '../../stripe/stripeError';
import { CreateChargeDto } from './dto/createCharge.dto';
import { ConfigService } from '@nestjs/config';
import { CustomerService } from '../customer/customer.service';

@Injectable()
export class CardsService {
  constructor(
    @Inject(STRIPE_TOKEN) private readonly stripe: Stripe,
    private readonly configService: ConfigService,
    private readonly customerService: CustomerService,
  ) {}

  public async attachCreditCard({
    paymentMethodId,
    customerId,
    hasDefaultCard,
  }: AttachCardsDto) {
    try {
      const entity = await this.stripe.setupIntents.create({
        customer: customerId,
        payment_method: paymentMethodId,
      });

      if (hasDefaultCard === false) {
        await this.setDefaultCreditCard({ paymentMethodId, customerId });
      }

      return {
        status: HttpStatus.OK,
        message: 'Card is attached success',
        entity,
      };
    } catch (e) {
      throw new NotFoundException(e.message);
    }
  }

  public async setDefaultCreditCard({
    paymentMethodId,
    customerId,
  }: SetDefaultCardDto) {
    try {
      const entity = await this.stripe.customers.update(customerId, {
        invoice_settings: {
          default_payment_method: paymentMethodId,
        },
      });

      return {
        status: HttpStatus.OK,
        message: 'Card is attached success',
        entity,
      };
    } catch (error) {
      if (error?.type === StripeError.InvalidRequest) {
        throw new BadRequestException('Wrong credit card chosen');
      }
      throw new InternalServerErrorException();
    }
  }

  public async listCreditCards({ customerId }: ListCreditCardsDto) {
    try {
      const { data } = await this.stripe.paymentMethods.list({
        customer: customerId,
        type: 'card',
        limit: 10,
      });
      return {
        status: HttpStatus.OK,
        message: 'Cards fetched',
        entities: data,
      };
    } catch (e) {
      throw new NotFoundException(e.message);
    }
  }

  public async deleteCard({
    paymentMethodId,
    defaultPaymentMethodId,
  }: DetachCardDto) {
    if (paymentMethodId === defaultPaymentMethodId) {
      throw new NotAcceptableException('Can not delete default card');
    }
    try {
      await this.stripe.paymentMethods.detach(paymentMethodId);
      return {
        status: HttpStatus.OK,
        message: 'Card detached',
      };
    } catch (e) {
      throw new NotFoundException(e.message);
    }
  }

  public async createCharge({
    customerId,
    paymentMethodId,
    amount,
  }: CreateChargeDto) {
    return this.stripe.paymentIntents.create({
      amount,
      customer: customerId,
      payment_method: paymentMethodId,
      currency: this.configService.get('STRIPE_CURRENCY'),
      off_session: true,
      confirm: true,
    });
  }
}
