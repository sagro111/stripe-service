import { Module } from '@nestjs/common';
import { CardsController } from './cards.controller';
import { CardsService } from './cards.service';
import { StripeModule } from '../../stripe/stripe.module';
import { ConfigModule } from '@nestjs/config';
import { CustomerModule } from '../customer/customer.module';

@Module({
  imports: [StripeModule, ConfigModule, CustomerModule],
  controllers: [CardsController],
  providers: [CardsService],
  exports: [CardsService],
})
export class CardsModule {}
