import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CardsService } from './cards.service';
import { AttachCardsDto } from './dto/attachCards.dto';
import { SetDefaultCardDto } from './dto/defaultCard.dto';
import { CreateChargeDto } from './dto/createCharge.dto';
import { DetachCardDto } from './dto/detachCard.dto';

@Controller('cards')
export class CardsController {
  constructor(private readonly cardsService: CardsService) {}

  @Post()
  async attachCreditCard(@Body() body: AttachCardsDto) {
    return this.cardsService.attachCreditCard(body);
  }

  @Post('default')
  async setDefaultCreditCard(@Body() body: SetDefaultCardDto) {
    return this.cardsService.setDefaultCreditCard(body);
  }

  @Get(':customerId')
  async listCreditCards(@Param('customerId') customerId: string) {
    return this.cardsService.listCreditCards({ customerId });
  }

  @Post('delete')
  async deleteCard(@Body() body: DetachCardDto) {
    return this.cardsService.deleteCard(body);
  }

  @Post('charge')
  async createCharge(@Body() body: CreateChargeDto) {
    return this.cardsService.createCharge(body);
  }
}
