import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { UpdateSubscriptionDto } from './dto/updateSubscription.dto';
import { SubscriptionsService } from './subscriptions.service';
import { CreateSubscriptionDto } from './dto/createSubscription.dto';

@Controller('subscriptions')
export class SubscriptionsController {
  constructor(private readonly subscriptionsService: SubscriptionsService) {}

  @Put()
  async update(@Body() body: UpdateSubscriptionDto) {
    return this.subscriptionsService.update(body);
  }

  @Post()
  async create(@Body() body: CreateSubscriptionDto) {
    return this.subscriptionsService.create(body);
  }

  @Get(':customerId')
  async get(@Param('customerId') customerId: string) {
    return this.subscriptionsService.get(customerId);
  }

  @Delete(':subscriptionId')
  async cancel(@Param('subscriptionId') subscriptionId: string) {
    return this.subscriptionsService.cancel(subscriptionId);
  }
}
