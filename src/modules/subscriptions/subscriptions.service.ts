import {
  BadRequestException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UpdateSubscriptionDto } from './dto/updateSubscription.dto';
import { CreateSubscriptionDto } from './dto/createSubscription.dto';
import { Stripe } from 'stripe';
import { STRIPE_TOKEN } from '../../stripe/stripe.module';

@Injectable()
export class SubscriptionsService {
  constructor(@Inject(STRIPE_TOKEN) private readonly stripe: Stripe) {}

  public async cancel(subscriptionId: string) {
    try {
      return await this.stripe.subscriptions.del(subscriptionId);
    } catch (e) {
      throw new NotFoundException(e.message);
    }
  }

  public async get(customerId: string) {
    const subscriptions = await this.listSubscriptions(customerId);

    const entity = subscriptions.data.find(({ status }) => status === 'active');

    if (entity === undefined) {
      return new NotFoundException('Customer not subscribed');
    }

    return {
      status: 200,
      message: 'Subscription fetched',
      entity,
    };
  }

  async create({ customerId, priceId }: CreateSubscriptionDto) {
    const subscriptions = await this.listSubscriptions(customerId);
    const inactiveSubscriptions = subscriptions.data.find(
      ({ status }) => status !== 'active',
    );

    if (inactiveSubscriptions !== undefined) {
      await this.cancel(inactiveSubscriptions.id);
    }

    try {
      const subscription = await this.stripe.subscriptions.create({
        customer: customerId,
        items: [
          {
            price: priceId,
          },
        ],
        payment_behavior: 'default_incomplete',
        expand: ['latest_invoice.payment_intent'],
      });

      if (
        typeof subscription.latest_invoice !== 'string' &&
        typeof subscription.latest_invoice.payment_intent !== 'string'
      ) {
        return {
          status: HttpStatus.OK,
          message: 'Subscription created',
          entity: subscription.latest_invoice.payment_intent.client_secret,
        };
      }
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  async update({ subscriptionId, paymentMethodId }: UpdateSubscriptionDto) {
    try {
      // await this.stripeService.attachCard({ customerId, paymentMethodId });
      const entity = await this.updateSubscription({
        subscriptionId,
        paymentMethodId,
      });

      return {
        status: HttpStatus.OK,
        message: 'Subscription updated',
        entity,
      };
    } catch (error) {
      return new NotFoundException(error.message);
    }
  }

  private async updateSubscription({
    subscriptionId,
    paymentMethodId,
  }: UpdateSubscriptionDto) {
    const prorationDate = Math.floor(Date.now() / 1000);

    const subscription = await this.stripe.subscriptions.retrieve(
      subscriptionId,
    );

    try {
      return await this.stripe.subscriptions.update(subscriptionId, {
        cancel_at_period_end: false,
        proration_behavior: 'always_invoice',
        default_payment_method: paymentMethodId,
        proration_date: prorationDate,
        items: [
          {
            id: subscription.items.data[0].id,
            price: 'price_1Kr2NMLtn7wFsYJV7ERFPiCf',
          },
        ],
      });
    } catch (e) {}
  }

  private async listSubscriptions(customer: string) {
    return this.stripe.subscriptions.list({ customer });
  }
}
