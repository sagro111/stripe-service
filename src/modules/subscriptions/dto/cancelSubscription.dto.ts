import { IsNotEmpty, IsString } from 'class-validator';

export class CancelSubscriptionDto {
  @IsNotEmpty()
  @IsString()
  subscriptionId: string;
}
