import { IsNotEmpty, IsString } from 'class-validator';

export class CreateSubscriptionDto {
  @IsNotEmpty()
  @IsString()
  customerId: string;
  @IsNotEmpty()
  @IsString()
  priceId: string;
}
