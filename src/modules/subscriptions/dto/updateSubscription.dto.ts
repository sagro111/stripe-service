import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateSubscriptionDto {
  @IsNotEmpty()
  @IsString()
  subscriptionId: string;

  @IsNotEmpty()
  @IsString()
  paymentMethodId: string;
}
