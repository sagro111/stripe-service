import {
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { CreateCustomerDto } from './dto/createCustomer.dto';
import { Stripe } from 'stripe';
import { STRIPE_TOKEN } from '../../stripe/stripe.module';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class CustomerService {
  constructor(
    @Inject(STRIPE_TOKEN) private readonly stripe: Stripe,
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  public async getCustomer(customerId: string) {
    try {
      const entity = await this.stripe.customers.retrieve(customerId);

      return {
        status: HttpStatus.OK,
        message: 'Customer fetched',
        entity,
      };
    } catch (e) {
      throw new NotFoundException(e.message);
    }
  }

  public async createCustomer({ email, name }: CreateCustomerDto) {
    try {
      const entity = await this.stripe.customers.create({ email, name });
      const userServiceUrl = this.configService.get('SERVICE_USER_URL');
      console.log(userServiceUrl);
      await this.httpService.post(`${userServiceUrl}/set-customer-id`, {
        customer_id: entity.id,
      });

      return {
        status: 200,
        message: 'Customer created',
        entity,
      };
    } catch (e) {
      return new NotFoundException(e.message);
    }
  }
}
