import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CreateCustomerDto } from './dto/createCustomer.dto';
import { CustomerService } from './customer.service';

@Controller('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Post()
  async createCustomer(@Body() body: CreateCustomerDto) {
    return this.customerService.createCustomer(body);
  }

  @Get(':customerId')
  async getCustomer(@Param('customerId') customerId: string) {
    return this.customerService.getCustomer(customerId);
  }
}
