import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { CustomerService } from './customer.service';
import { CustomerController } from './customer.controller';
import { StripeModule } from '../../stripe/stripe.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [StripeModule, HttpModule, ConfigModule],
  providers: [CustomerService],
  exports: [CustomerService],
  controllers: [CustomerController],
})
export class CustomerModule {}
