import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { rawBodyMiddleware } from './stripe/middleware/rawBody';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bodyParser: false });
  app.use(rawBodyMiddleware());
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();

  await app.listen(4242);
}

bootstrap();
