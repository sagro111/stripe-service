import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as Joi from '@hapi/joi';
import { CustomerModule } from './modules/customer/customer.module';
import { WebhookModule } from './webhook/webhook.module';
import { StripeModule } from './stripe/stripe.module';
import { SubscriptionsModule } from './modules/subscriptions/subscriptions.module';
import { CardsModule } from './modules/cards/cards.module';
import { ProductsModule } from './modules/products/products.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        STRIPE_SECRET_KEY: Joi.string(),
        STRIPE_CURRENCY: Joi.string(),
        FRONTEND_URL: Joi.string(),
        WEBHOOK_API_KEY: Joi.string(),
      }),
    }),
    StripeModule,
    CustomerModule,
    WebhookModule,
    SubscriptionsModule,
    CardsModule,
    ProductsModule,
  ],
  controllers: [],
})
export class AppModule {}
